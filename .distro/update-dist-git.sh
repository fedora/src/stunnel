#!/usr/bin/env bash

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd)

packit source-git \
	update-dist-git \
		--pkg-tool fedpkg \
		-F <(git -C "$SCRIPT_DIR/.." log --pretty="tformat:%B" -1) \
		--upstream-ref "$(git -C "$SCRIPT_DIR/.." describe --abbrev=0)" \
		"$@" \
		"$(readlink -f "$SCRIPT_DIR/..")" \
		"$(readlink -f "$SCRIPT_DIR/../../../rpm/stunnel")"
