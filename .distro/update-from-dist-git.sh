#!/usr/bin/env bash

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd)

packit source-git \
	update-source-git \
	"$(readlink -f "$SCRIPT_DIR/../../../rpm/stunnel")" \
	"$(readlink -f "$SCRIPT_DIR/..")"
